module.exports = [
    {
        title: 'ID',
        key: 'id'
    },
    {
        title: 'First Name',
        key: 'firstName'
    },
    {
        title: 'Last Name',
        key: 'lastName'
    },
    {
        title: 'E-mail',
        key: 'email'
    },
    {
        title: 'Phone',
        key: 'phone'
    }
];