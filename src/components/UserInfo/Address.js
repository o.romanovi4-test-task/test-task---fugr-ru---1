import React from 'react';

export default props => (
    <div className='user__address'>
        <div>Адрес проживания: <b>{props.address.streetAddress}</b></div>
        <div>Город: <b>{props.address.city}</b></div>
        <div>Провинция/штат: <b>{props.address.state}</b></div>
        <div>Индекс: <b>{props.address.zip}</b></div>
    </div>
);