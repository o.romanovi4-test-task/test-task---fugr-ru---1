import React from 'react';

export default props => (
    <div className='user__description'>
        Описание:
        <textarea defaultValue={props.children}/>
    </div>
)