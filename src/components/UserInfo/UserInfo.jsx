import React, { Component } from 'react';
import './UserInfo.css';

import Description from './Description';
import Address from './Address';

class UserInfo extends Component {
  render() {
    return (
      <div className='user' ref={this.props.refContainer}>
        Выбран пользователь <b>{this.props.firstName} {this.props.lastName}</b>
        <Description>{this.props.description}</Description>
        <Address address={this.props.address}/>
	    </div>
    );
  }
}

export default UserInfo;