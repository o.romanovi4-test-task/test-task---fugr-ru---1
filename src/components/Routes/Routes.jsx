import React, { Component } from 'react';
import { Route, Switch } from 'react-router';

import SimpleTableExample from '../../views/SimpleTable';
import AdvancedTableExample from '../../views/AdvancedTable';

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route path={`/simple`} component={SimpleTableExample}/>
        <Route path={`/advanced`} component={AdvancedTableExample}/>
      </Switch>
    );
  }
}

export default Routes;