import React from 'react';
import Cell from '../Cell';
import { ChevronDown, ChevronUp } from 'react-feather';
import PropTypes from 'prop-types';
import './style.css';

const getIconByDirection = (direction) => {
    switch (direction) {
        case 'asc': {
            return ChevronUp;
        }

        case 'desc': {
            return ChevronDown;
        }

        default: {
            return ChevronDown;
        }
    }
};

const SortableCell = ({...props}) => {
    const Direction = getIconByDirection(props.direction);

    return (
        <Cell onClick={props.onSortChange} {...props} style={{cursor: 'pointer'}} className='table__cell--sortable'>
            {props.children}
            <Direction />
        </Cell>
    );
};

SortableCell.propTypes = {
    direction: PropTypes.string.isRequired,
    onSortChange: PropTypes.func.isRequired
};

export default SortableCell;