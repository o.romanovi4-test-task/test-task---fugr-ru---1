import React, { Component } from 'react';
import './Body.css';

class Body extends Component {
  render() {
    return (
      <div className='table__body'>
        {this.props.children}
	    </div>
    );
  }
}

export default Body;