import React, { Component } from 'react';
import './Cell.css';

class Cell extends Component {
  render() {
    return (
      <div {...this.props} style={{width: this.props.width, ...this.props.style}} className={`table__cell ${this.props.className || ''}`}>
        {this.props.children}
	    </div>
    );
  }
}

export default Cell;