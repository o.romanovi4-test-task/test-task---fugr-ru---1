import React, { Component } from 'react';
import './Row.css';

class Row extends Component {
  render() {
    return (
      <div {...this.props} className={`table__row ${this.props.className || ''}`}>
        {this.props.children}
	    </div>
    );
  }
}

export default Row;