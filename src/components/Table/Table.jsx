import React, { Component } from 'react';
import './Table.css';

class Table extends Component {
  render() {
    return (
      <div ref={this.props.tableRef} className={`table ${this.props.className || ''}`}>
        {this.props.children}
	    </div>
    );
  }
}

export default Table;