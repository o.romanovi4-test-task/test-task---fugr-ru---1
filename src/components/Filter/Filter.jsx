import React from 'react'
import PropTypes from 'prop-types';
import './styles.css'

class Filter extends React.Component {
  constructor() {
    super();
    this.filterText = React.createRef();
  }

  filterTextChanged = (e) => {
    e.preventDefault();

    if (this.props.onFilterTextChanged) {
      this.props.onFilterTextChanged(this.filterText.current.value);
    }
  }

  render() {
    return (
      <form onSubmit={this.filterTextChanged} className="filter">
        <input ref={this.filterText} type="text" placeholder='Поиск...'/>
        <button>Найти</button>
      </form>
    );
  }
}

Filter.propTypes = {
  onFilterTextChanged: PropTypes.func.isRequired
};

export default Filter
    