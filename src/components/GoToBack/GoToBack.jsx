import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ArrowLeft } from 'react-feather';

class GoToBack extends Component {
  render() {
    return (
      <Link style={{textDecoration: 'none'}} to='/'> 
        <ArrowLeft/> Назад
      </Link>
    );
  }
}

export default GoToBack;