import React from 'react';
import './Loader.css';

const Loader = (WrappedComponent) => {
  return ({isLoading = false, ...props}) => {
    if (isLoading) {
      return (<h4>Загрузка...</h4>);
    } else {
      return (<WrappedComponent {...props}/>)
    }
  };
}

export default Loader;