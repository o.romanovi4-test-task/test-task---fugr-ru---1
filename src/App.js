import React, { Component } from 'react';
import './App.css';
import Routes from './components/Routes';

import { Link } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className='example-switcher'>
          <Link to='/simple'><button>Маленький объем данных</button></Link>
          <Link to='/advanced'><button>Большой объем данных</button></Link>
        </div>

        <Routes />
      </div>
    );
  }
}

export default App;
