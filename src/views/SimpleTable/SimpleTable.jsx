import React, { Component } from 'react';
import './SimpleTable.css';
import ResponseTable from '../../containers/ResponseTable';

const { simple } = require('../../config/urls.js');

class SimpleTable extends Component {
  render() {
    return (
      <ResponseTable 
        currentExample='маленький' 
        className='simple-table-expample' 
        apiUrl={simple}
      />
    );
  }
}

export default SimpleTable;