import React, { Component } from 'react';
import './AdvancedTable.css';

import ResponseTable from '../../containers/ResponseTable';
const { advanced } = require('../../config/urls');

class AdvancedTable extends Component {
  render() {
    return (
      <ResponseTable 
        currentExample='большой' 
        className='advanced-table-expample' 
        apiUrl={advanced}
      />
    );
  }
}

export default AdvancedTable;