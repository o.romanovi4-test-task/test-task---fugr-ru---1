import React, { Component } from 'react';
import Page from './components/Page';
import PropTypes from 'prop-types';
import './styles.css';

class Paginator extends Component {
    onPageChange(page) {
        if (this.props.onPageChange) {
            if (page !== this.props.currentPage) {
                this.props.onPageChange(page);
            }
        }
    }

    render() {
        const pages = Array.from(Array(this.props.totalPages), (num, index) => index + 1);

        return (
            <div className='paginator'>
                {
                    pages.map((page, index) => (
                        <Page
                            onClick={() => { this.onPageChange(page) }} 
                            page={page} 
                            key={index} 
                            selected={page === this.props.currentPage}
                        />
                    ))
                }
            </div>
        );
    }
}

Paginator.propTypes = {
    totalPages: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    onPageChange: PropTypes.func
};

export default Paginator;