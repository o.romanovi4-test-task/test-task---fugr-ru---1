import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const Page = ({...props}) => (
    <div {...props} className={`paginator__page ${props.selected ? 'paginator__page--selected': ''}`}>
        {props.page}
    </div>
);

Page.propTypes = {
    page: PropTypes.number.isRequired,
    selected: PropTypes.bool
};

export default Page;