import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, TableHeader, TableBody, TableRow, TableSortableCell } from '../../components/Table';
import GoToBack from '../../components/GoToBack';
import './ResponseTable.css';

import Filter from '../../components/Filter';
import UserInfo from '../../components/UserInfo';
import Paginator from '../Paginator';

import ListData from '../ListData';
import Loader from '../../components/Loader';
const ListDataWithLoader = Loader(ListData);

const headerCells = require('../../config/header-cells');

class ResponseTable extends Component {
  state = {
    loading: true,
    err: '',
    data: {
      original: [],       // Массив со всеми данными
      printed: [],        // Массив с данными, который будет выводиться на страницу
      page: []            // Массив с выборкой данных постранично
    },

    pagination: {
      count: 0,
      perPage: 50,
      currentPage: 1
    },

    expandedData: {
      status: false,
      item: null
    },

    filter: {
      text: '',
      data: null          // Массив с отсортированными данными
    },

    sort: {
      direction: 'asc',
      sortedColumnIndex: 0
    }
  };

  constructor() {
    super();
    this.tableWithDataRef = React.createRef();
    this.expandDataRef = React.createRef();
  }
  
  componentDidMount() {
    fetch(this.props.apiUrl)
      .then(response => response.json())
      .then(responseData => {
        const { pagination, data } = this.state;
        pagination.count = Math.ceil(responseData.length / pagination.perPage);
        data.original = responseData;
        data.printed = data.page = responseData.slice(0, pagination.perPage - 1)
        this.setState({ data, pagination }, () => {
          this.onSortChanged(0, 'asc');
        });
      })
      .catch(err => { this.setState({ err }) })
      .finally(() => { this.setState({ loading: false }) });
  }

  /**
   * Обработчик для клика по строке
   */
  dataExpandHandle = (item) => {
    const { expandedData } = this.state;
    const newExpandedData = {
      status: false,
      item: null
    } 

    // Если дополнительная информация не показывается или новый объект отличается от того, что есть в состоянии
    if (!expandedData.status || expandedData.item.id !== item.id) {
      newExpandedData.status = true;
      newExpandedData.item = item;
    }

    this.setState({ expandedData: newExpandedData }, () => {
      this.expandDataRef.current.scrollIntoView({ behavior: 'smooth' });
    });
  }

  /**
   * Обработчик на обновление фильтра
   * @param { string } newFilterText Подстрока, которую нужно найти
   * @param { boolean } forceUpdate Нужно ли принудительно обновить. В случае, если newFilterText совпадает с текущим, но все равно нужно обновить - следует поставить true
   */
  filterChanged = (newFilterText, forceUpdate = false) => {
    const { filter, data, pagination } = this.state;

    if (filter.text === newFilterText && !forceUpdate) 
      return;

    pagination.currentPage = 1;
    if (newFilterText === '') {
      filter.text = '';
      filter.data = null;
      this.setState({ filter, pagination }, this.updatePrintedData);
      return;
    } 

    filter.text = newFilterText;
    filter.data = data.original.filter(item => {
      let found = false;
      for (let i = 0; i < headerCells.length; i++) {
        const value = item[headerCells[i].key].toString();
        if (value.includes(newFilterText)) {
          found = true;
          break;
        }
      }

      return found;
    });

    this.setState({ filter, pagination }, this.updatePrintedData);
  };

  /**
   * Обновление данных на вывод. Выполняет фильтрацию и пагинацию
   */
  updatePrintedData = () => {
    const { data, filter, pagination } = this.state;

    // Обработка пагинации  
    const startIndex = (pagination.currentPage - 1) * pagination.perPage;
    const endIndex = startIndex + pagination.perPage;

    const processData = (arrayWithData) => {
      data.printed = data.page = arrayWithData.slice(startIndex, endIndex);
      pagination.count = Math.ceil(arrayWithData.length / pagination.perPage);
    };

    // Обработка фильтрации
    if (filter.data) processData(filter.data);
    else processData(data.original);

    this.setState({ data, pagination }, () => {
      window.scrollTo(0, this.tableWithDataRef.current.offestTop);
    });
  };

  /**
   * Обработчик для смены страницы
   * @param { number } page Номер страницы
   */
  onPageChange = (page) => {
    const { pagination } = this.state;
    pagination.currentPage = page;
    this.setState({ pagination }, this.updatePrintedData);
  };

  /**
   * Обработчик для измение направления сортировки
   * @param { number } columnIndex Индекс сортированной колонки
   * @param { string } newDirection Направление сортирвки (asc - по убыванию, desc - по возрастанию)
   */
  onSortChanged = (columnIndex, newDirection) => {
      const { sort, data, filter } = this.state;

      const sortAsc = () => {
        data.original.sort((a, b) => {
          if (a[headerCells[columnIndex].key] > b[headerCells[columnIndex].key])
            return 1;
  
          if (a[headerCells[columnIndex].key] < b[headerCells[columnIndex].key])
            return -1;
  
          return 0;
        });
      };

      const sortDesc = () => {
        data.original.sort((a, b) => {
          if (a[headerCells[columnIndex].key] < b[headerCells[columnIndex].key])
            return 1;
  
          if (a[headerCells[columnIndex].key] > b[headerCells[columnIndex].key])
            return -1;
  
          return 0;
        });
      };

      sort.direction = newDirection;
      sort.sortedColumnIndex = columnIndex;

      switch(sort.direction) {
        case 'asc': {
          sortAsc();
          break;
        }

        case 'desc': {
          sortDesc();
          break;
        }

        default: {
          sortDesc();
        }
      }
      
      this.setState({ sort, data }, () => {        
        if (filter.data) this.filterChanged(filter.text, true);
        else this.updatePrintedData();
      });
  };

  render() {
    const { pagination, sort } = this.state;

    return (
      <div className={this.props.className}>
        <GoToBack />
        <h4>Текущий пример: {this.props.currentExample} объем данных</h4>
        {!this.state.loading ? <Filter onFilterTextChanged={this.filterChanged}/> : null}

        <Table tableRef={this.tableWithDataRef}>
          <TableHeader>
            <TableRow>
              {
                headerCells.map((cell, index) => {
                  const direction = sort.sortedColumnIndex !== -1 && sort.sortedColumnIndex === index ? sort.direction : 'desc';

                  return (
                    <TableSortableCell
                      key={index}
                      width='100%'
                      direction={direction}
                      onSortChange={() => { this.onSortChanged(index, direction === 'asc' ? 'desc' : 'asc') }}
                    >
                      {cell.title}
                    </TableSortableCell>
                  )
                })
              }
            </TableRow>
          </TableHeader>

          <TableBody>
            <ListDataWithLoader isLoading={this.state.loading} data={this.state.data.printed} dataExpandHandle={this.dataExpandHandle}/>
          </TableBody>
        </Table>

        <Paginator
          totalPages={pagination.count}
          currentPage={pagination.currentPage}
          onPageChange={this.onPageChange}
        />

        {this.state.expandedData.status ? (<UserInfo refContainer={this.expandDataRef} {...this.state.expandedData.item}/>) : null}
	    </div>
    );
  }
}

Response.propTypes = {
  apiUrl: PropTypes.string.isRequired,
  currentExample: PropTypes.string.isRequired
};
    
export default ResponseTable;