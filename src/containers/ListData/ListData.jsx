import React, { Component } from 'react';
import './ListData.css';
import { TableCell, TableRow } from '../../components/Table';

class ListData extends Component {
  dataExpandHandler(item) {
    if (this.props.dataExpandHandle) this.props.dataExpandHandle(item);
  }

  render() {
    const { data } = this.props;
    return data.map((item, index) => (
      <TableRow style={{cursor: 'pointer'}} onClick={() => {this.dataExpandHandler(item)}} key={index}>
        <TableCell width='100%'>{item.id}</TableCell>
        <TableCell width='100%'>{item.firstName}</TableCell>
        <TableCell width='100%'>{item.lastName}</TableCell>
        <TableCell width='100%'>{item.email}</TableCell>
        <TableCell width='100%'>{item.phone}</TableCell>
      </TableRow>
    ))
  }
}

export default ListData;